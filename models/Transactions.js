const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const TransactionSchema = new Schema({
	userId : {
		type : String,
		required : true
	},
	bookingCode : {
		type : String,
		required : true,
		unique : true
	},
	status : {
		type : String,
		default : "Booked"
	},
	dateCreated : {
		type : Date,
		default : Date.now
	},
	paymentMode : {
		type : String,
		required : true
	},
	total : {
		type : Number,
		required : true
	},
	reservationDate : {
		type : Date,
		required : true
	},
	timeIn : {
		type : String,
		required : true
	},
	timeOut : {
		type : String,
		required : true
	},
	roomId : {
		type : String,
		required : true
	}



});

const Transaction = mongoose.model('Transaction', TransactionSchema);
module.exports = Transaction;