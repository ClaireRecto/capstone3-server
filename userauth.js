module.exports = function (req,res,next){
	if(req.user.role == 'user'){
		next();
	} else {
		res.send("You are not authorized to access this")
	}
}