const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');

// initialize server
const app = express();
const port = process.env.PORT || 8000;


//connect DB to local
// mongoose.connect('mongodb://localhost/bookingsystem',() => {
// 	console.log("Connected to database")
// });

//connect DB to heroku
mongoose.connect('mongodb+srv://adminname:test1234@cluster0-w7vo6.mongodb.net/test?retryWrites=true&w=majority',() => {
	console.log("Connected to database")
});

// middlewares
app.use(passport.initialize());
app.use(cors());
app.use('/public',express.static('public/rooms'));
app.use(bodyParser.json());

//routes
app.use('/users', require('./routes/users'));
app.use('/categories', require('./routes/categories'));
app.use('/rooms', require('./routes/rooms'));
app.use('/transactions', require('./routes/transactions'));


// error handling middleware
app.use(function(err,req,res,next){
	console.log(err.message)
	res.status(400).json({
		error : err.message
	})
})
app.listen(port, ()=> {
	console.log(`Listening in port ${port}`)
})
