const router = require('express').Router();
const Room = require('../models/Rooms');
const Category = require('../models/Categories');
const multer = require('multer');
const auth = require('./../auth');


const storage = multer.diskStorage({
	destination : function(req,file,cb){
		cb(null,"public/rooms")
	},
	filename : function(req,file,cb){
		cb(null, Date.now() + "-" + file.originalname)
	}
})
const upload = multer({storage : storage});
const passport = require('passport');

//index
router.get('/', function(req,res,next){
	Room.find()
	.then(rooms=>{
		Category.find()
		.then(categories => {
			rooms.map(room => {
				categories.forEach(category => {
					if(room.categoryId == category._id){
						room.categoryId = category.name
					}
				})
				return
			})
			res.send(rooms)
		})
		
	})
	.catch(next)
});

//single
router.get('/:id', (req,res,next) => {
	Room.findOne({_id: req.params.id})
	.then(room=> res.json(room))
	.catch(next)
});


//create
router.post('/', 
	passport.authenticate('jwt', { session: false}), auth,
	upload.single('image'), 
	(req,res,next) => {
		req.body.image = "/public/" + req.file.filename;
		Room.create(req.body)
		.then(room=>res.json(room))
		.catch(next)
})

//put
router.put('/:id', 
	passport.authenticate('jwt', { session: false}), auth,
	upload.single('image'), 
	(req,res,next) => {
		let update = {
			...req.body
		}

		if (req.file){
			update = {
				...req.body,
				image : "/public/" + req.file.filename
			}
		}
		
		Room.findByIdAndUpdate(req.params.id, update, {new:true})
		.then(room => res.json(room))
		.catch(next);
	}
)

//delete
router.delete('/:id', 
	passport.authenticate('jwt', { session: false}), auth,
	upload.single('image'), 
	(req,res,next) => {
		Room.findOneAndDelete({_id:req.params.id})
		.then(room=>res.json(room))
		.catch(next)
})

module.exports =  router;
