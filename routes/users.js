const router = require('express').Router();
const User = require('../models/Users');
const bcrypt = require('bcrypt');
const passport = require('passport');
require('./../passport-setup');
const jwt = require('jsonwebtoken');


router.post('/register', (req,res,next) => {
	let firstname = req.body.firstname;
	let lastname = req.body.lastname;
	let email = req.body.email;
	let password = req.body.password;
	let confirmPassword = req.body.confirmPassword;

	if(!firstname || !lastname || !email || !password || !confirmPassword){
		return res.status(400).send({
			message : "Incomplete fields"
		})
	}

	if(password.length < 8){
		return res.status(400).send({
			message : "Password must be atleast 8 characters"
		})
	}

	if(password !== confirmPassword){
		return res.status(400).send({
			message : "Password do not match"
		})
	}

	User.findOne({email : email})
	.then(user => {
		if(user){
			return res.status(400).send({
				message : "Email already in use"
			})
		} else {
			const saltRounds = 10;
			bcrypt.genSalt(saltRounds, function(err,salt){
				bcrypt.hash(password, salt, function(err,hash){
					User.create({
						firstname,
						lastname,
						email,
						password : hash
					})
					.then(user => {
						return res.send({
							user, 
							message : "You are now registered!"
						})
					})
				})
			})
		}
	})
})

router.post('/profile',passport.authenticate('jwt',{session : false}), (req,res) => {
		res.send(req.user)
	})


router.post('/login',(req,res,next) => {
	let email = req.body.email;
	let password = req.body.password;

	if(!email || !password){
		return res.status(400).send({
			message : "Something went wrong, please try again"
		})
	}

	User.findOne({email})
	.then(user => {
		if(!user){
			return res.status(400).send({
				message : "Something went wrong, please try again"
			})
		} else {
			bcrypt.compare(password, user.password, (err, passwordMatch) => {
				if(passwordMatch){
					let token = jwt.sign({id : user._id}, 'secret')
					return res.send({
						message : "Login successful",
						token : token, 
						user : {
							firstname : user.firstname,
							lastname : user.lastname,
							role : user.role,
							id : user._id
						}
					})
				}
			})
		}
	})
})

module.exports = router;