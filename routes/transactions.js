const router = require('express').Router();
const Room = require('./../models/Rooms');
const Transaction = require('./../models/Transactions');
const User = require('./../models/Users');
const auth = require('./../auth');
const passport = require('passport');
const userauth = require('./../userauth');

const stripe = require('stripe')("sk_test_gl9zuOyMO5e1UzJzIwATrHBD00Uf2PM3zI");

router.get('/', 
	passport.authenticate('jwt',{session:false}),
	(req,res,next) => {
	if(req.user.role === 'admin'){
		
			Transaction.find()
			.then(transactions => {
				User.find()
				.then(users => {
					transactions.map(transaction => {
						users.forEach(user => {
							if(transaction.userId == user._id){
								transaction.userId = user.firstname + " " + user.lastname;
							}

						})
						return
					})
					res.send(transactions)
				})
			})

		
	} else {
		Transaction.find({userId : req.user.id})
		.then(transactions => {
			User.find().then(users => {
				transactions.map(transaction => {
					users.forEach(user => {
						if(transaction.userId == user._id){
							transaction.userId = user.firstname + " " + user.lastname
						}
					})
					return
				})
				res.send(transactions)
			})
		})
	}
})

router.post('/',
	passport.authenticate('jwt',{session:false}), userauth,
	(req,res,next) => {
		let transaction = {
			userId : req.user._id,
			bookingCode : Date.now(),
			reservationDate : req.body.reservationDate,
			timeIn : req.body.timeIn,
			timeOut : req.body.timeOut,
			roomId : req.body.roomId,
			paymentMode : req.body.paymentMode,
			total : req.body.total
		};
		Transaction.create(transaction)
		.then(transaction => {
			 res.json(transaction)
		})
		.catch(next)

	}
)

// router.post('/', passport.authenticate('jwt',{session : false}), (req,res,next) => {
// 	let orders = req.body.orders;
// 	let orderIds = orders.map( room => {
// 		return room.id
// 	})

// 	Room.find({_id : orderIds})
// 	.then(rooms => {
// 		let total = 0;
// 		let newRooms = rooms.map( room => {
// 			let matchedRoom = {};
// 			orders.map( order => {
// 				if (room.id === order.id) {
// 					matchedRoom = {
// 						roomId : room._id,
// 						name : room.name,
// 						price : room.price,
// 						capacity : room.capacity,
// 						subtotal : order.qty * room.price
// 					}
// 				}
// 			})
// 			total += matchedRoom.subtotal
// 			return matchedRoom;
// 		})

// 		let transaction = {
// 			userId : req.user._id,
// 			transactionCode : Date.now(),
// 			total,
// 			rooms : newRooms
// 		}

// 		// return res.send(transaction)
// 		Transaction.create(transaction)
// 		.then(transaction => {
// 			return res.send(transaction)
// 		})
// 	})
// })

router.put('/:id', (req,res,next) => {
	Transaction.findByIdAndUpdate(
		req.params.id, 
		{ 
			status : req.body.status
		}, 
		{
			new : true
		}
	)
	.then(transaction => res.send(transaction))
})

// start of stripe

router.post('/stripe',(req,res,next) => {

	let total = req.body.total;
	
	User.findOne({_id : req.body.userId})
	.then( user => {
		if(!user) {
			res.status(500).send({message: 'Incomplete'})
		} else {
			if(!user.stripeCustomerId){
	            stripe.customers
	            .create({
	                email: user.email,
	            })
	            .then(customer => {
	                return User.findByIdAndUpdate({ _id: user.id},{stripeCustomerId : customer.id}, {new:true})
	            })
	            .then( user => {
	                return stripe.customers.retrieve(user.stripeCustomerId)
	            })
	            .then((customer) => {
	                return stripe.customers.createSource(customer.id, {
	                source: 'tok_visa',
	                });
	            })
	            .then((source) => {
	                return stripe.charges.create({
	                amount: total,
	                currency: 'usd',
	                customer: source.customer,
	                });
	            })
	            .then((charge) => {
	                // New charge created on a new customer
	                console.log(charge)

	                res.send(charge);
	            })
	            .catch((err) => {
	                // Deal with an error
	                res.send(err)
	            });
	        } else {
                stripe.customers.retrieve(user.stripeCustomerId)
                .then((customer) => {
                    return stripe.customers.createSource(customer.id, {
                    source: 'tok_visa',
                    });
                })
                .then((source) => {
                    return stripe.charges.create({
                    amount: total * 100, //multiply by 100 for centavos
                    currency: 'usd',
                    customer: source.customer,
                    });
                })
                .then((charge) => {
                    // New charge created on a new customer
                    // console.log(charge)
                    res.send(charge);
                })
                .catch((err) => {
                    // Deal with an error
                    res.send(err)
                });
            }


		}
	})

})

// end of stripe

module.exports = router;