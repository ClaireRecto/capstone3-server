const router = require('express').Router();
const passport = require('passport');
const Category = require('./../models/Categories');
const auth = require('./../auth');

//index
router.get('/', function(req,res,next){
	Category.find()
	.then(categories => {
		res.json(categories)
	})
	.catch(next);
});

//single
router.get('/:id', 
	(req,res,next) => {
		Category.findOne({
			_id: req.params.id
		})
		.then(category=> res.json(category))
		.catch(next);
	}
);

// create
router.post('/', 
	passport.authenticate('jwt',{session : false}), auth,
	(req,res,next) => {
		Category.create(req.body)
		.then( (category) => {
			res.send(category)
		})
		.catch(next)
})

//put
router.put('/:id', 
	passport.authenticate('jwt',{session : false}), auth,
	(req,res,next) => {
	Category.findOneAndUpdate(
		{
			_id: req.params.id
		},
		{
			name: req.body.name
		},
		{
			new : true,

		})
	.then(category=>res.json(category))
	.catch(next)
})

//delete
router.delete('/:id', 
	passport.authenticate('jwt',{session : false}), auth,
	(req,res,next) => {
		Category.findOneAndDelete({
			_id: req.params.id
		})
		.then(category=>res.json(category))
		.catch(next)
})

module.exports =  router;
